# Get base image
FROM ubuntu:20.04

#Non Interactive Node in shell
ENV DEBIAN_FRONTEND=noninteractive

########################################################
##SYSTEM
########################################################
# Install Java
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y software-properties-common
RUN apt-get install -y apt-utils
RUN add-apt-repository ppa:openjdk-r/ppa -y
RUN apt-get install -y openjdk-8-jdk
RUN apt-get clean
     
# Install needed packages
RUN apt-get install less -y
RUN apt-get -y install vim
RUN apt-get -y install ssh
RUN apt-get -y install openssh-server
RUN apt-get -y install openssh-client
RUN apt-get -y install rsync
RUN apt-get update && \
      apt-get -y install sudo

# Install Ping
RUN echo "Installing Ping"
RUN apt-get update && \
    apt-get install -y iputils-ping
RUN echo "Installed Ping"

# Install pip
RUN apt-get -y install python3.9

# Create user agricore
RUN useradd -ms /bin/bash agricore
USER agricore
WORKDIR /home/agricore


ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/jre/

########################################################
##HADOOP
########################################################
#environment variables
ENV HADOOP_VERSION 3.2.1
ENV HADOOP_HOME /home/agricore/hadoop-$HADOOP_VERSION
ENV HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
ENV PATH $PATH:$HADOOP_HOME/bin

RUN wget http://archive.apache.org/dist/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz  -q -O ./hadoop-$HADOOP_VERSION.tar.gz
RUN tar -xvzf ./hadoop-$HADOOP_VERSION.tar.gz
RUN echo "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre/" >> $HADOOP_HOME/etc/hadoop/hadoop-env.sh

#Create folder hadoop logs
RUN mkdir -p $HADOOP_HOME/logs
#Create folder hdfs
RUN mkdir -p /tmp/hadoop-agricore/dfs/data

########################################################
##SPARK
########################################################
#environment variables
ENV SPARK_VERSION 3.2.2
ENV SPARK_HOME /home/agricore/spark/
ENV SPARK_CONF_DIR $SPARK_HOME/conf
ENV SPARK_DIST_CLASSPATH="$HADOOP_HOME/etc/hadoop/*:$HADOOP_HOME/share/hadoop/common/lib/*:$HADOOP_HOME/share/hadoop/common/*:$HADOOP_HOME/share/hadoop/hdfs/*:$HADOOP_HOME/share/hadoop/hdfs/lib/*:$HADOOP_HOME/share/hadoop/hdfs/*:$HADOOP_HOME/share/hadoop/yarn/lib/*:$HADOOP_HOME/share/hadoop/yarn/*:$HADOOP_HOME/share/hadoop/mapreduce/lib/*:$HADOOP_HOME/share/hadoop/mapreduce/*:$HADOOP_HOME/share/hadoop/tools/lib/*"
ENV PATH $PATH:${SPARK_HOME}/bin:${SPARK_HOME}/sbin

RUN wget https://dlcdn.apache.org/spark/spark-${SPARK_VERSION}/spark-${SPARK_VERSION}-bin-hadoop3.2.tgz -q -O /tmp/spark-${SPARK_VERSION}-bin-hadoop3.2.tgz
RUN tar -xvzf /tmp/spark-${SPARK_VERSION}-bin-hadoop3.2.tgz --directory ~
RUN mv ~/spark-${SPARK_VERSION}-bin-hadoop3.2 spark

ADD conf-master/hive-site.xml $SPARK_CONF_DIR/

########################################################
##SQOOP
########################################################
#environment variables
ENV SQOOP_HOME /home/agricore/sqoop-1.4.7.bin__hadoop-2.6.0/
ENV PATH $PATH:$SQOOP_HOME/bin

RUN wget https://archive.apache.org/dist/sqoop/1.4.7/sqoop-1.4.7.bin__hadoop-2.6.0.tar.gz
RUN tar -xvzf ./sqoop-1.4.7.bin__hadoop-2.6.0.tar.gz

########################################################
##HIVE
########################################################
#environment variables
ENV HIVE_VERSION=3.1.2
ENV HIVE_HOME /home/agricore/apache-hive-${HIVE_VERSION}-bin
ENV HIVE_CONF_DIR="${HIVE_HOME}/conf"
ENV PATH "${PATH}:${HIVE_HOME}/bin"
RUN wget https://archive.apache.org/dist/hive/hive-${HIVE_VERSION}/apache-hive-${HIVE_VERSION}-bin.tar.gz
RUN tar -xvzf ./apache-hive-${HIVE_VERSION}-bin.tar.gz
# RUN chown -R root:root $HIVE_HOME 
RUN mkdir -p $HIVE_HOME/hcatalog/var/log 
RUN mkdir -p $HIVE_HOME/var/log 
RUN mkdir -p $HIVE_HOME/meta
RUN mkdir -p $HIVE_HOME/warehouse
RUN mkdir -p $HIVE_CONF_DIR 
RUN chmod 777 $HIVE_HOME/hcatalog/var/log 
RUN chmod 777 $HIVE_HOME/var/log
RUN chmod 777 $HIVE_HOME/warehouse
RUN chmod 777 $HIVE_HOME/meta

ADD conf-master/hive-site.xml $HIVE_CONF_DIR/

########################################################
##ZEPPELIN
########################################################
#environment variables
ENV ZEPPELIN_VERSION=0.10.1
ENV ZEPPELIN_HOME /home/agricore/zeppelin-${ZEPPELIN_VERSION}-bin-all



########################################################
##PORTS
########################################################
EXPOSE 8040 
EXPOSE 8042
EXPOSE 22
EXPOSE 8030
EXPOSE 8031
EXPOSE 8032
EXPOSE 8033
EXPOSE 8088
EXPOSE 10020
EXPOSE 19888
EXPOSE 10000
EXPOSE 8080 
EXPOSE 8083
EXPOSE 50070
EXPOSE 6789