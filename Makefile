help:
	@echo "Available commands are:"
	@echo "- build_base_image   - builds shared image"
	@echo "- build_master_image - builds master image"
	@echo "- build_worker_image  - builds worker image"

all: build_base_image build_master_image build_worker_image build_jupyter_image

build_base_image:
	docker rmi --force spark_base_image; 
	docker build -f ./Dockerfile . -t spark_base_image

build_master_image:
	docker rmi --force spark_compose_master; 
	docker build -f ./master/Dockerfile . -t spark_compose_master

build_worker_image:
	docker rmi --force spark_compose_worker; 
	docker build -f ./worker/Dockerfile . -t spark_compose_worker

build_jupyter_image:
	docker rmi --force spark_compose_jupyter;
	docker build -f ./jupyter/Dockerfile . -t spark_compose_jupyter