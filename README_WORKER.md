# Config

##capacity-scheduler.xml


##core-site.xml

- fs.default.name - 'hdfs://agricoredwh-master:9000':  the address of the NameNode and all the HDFS command refers to the this NameNode address.

##hadoop-policy.xml

##hdfs-site.xml

##yarn-site.xml

- yarn.log-aggregation-enable - 'true': 
- yarn.resourcemanager.hostname - 'agricoredwh-master': The host name of the ResourceManager. Must be set for all RM
- yarn.nodemanager.remote-app-log-dir - '/tmp/logs': Where to aggregate logs to