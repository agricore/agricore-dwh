#!/bin/bash

echo "Initializing SSH"
sudo service ssh start

eval `ssh-agent -s`
exec ssh-add &

# Below 3 lines will hide the prompt about fingerprinting during ssh connection 
# to HDFS
ssh -oStrictHostKeyChecking=no agricoredwh-master uptime
ssh -oStrictHostKeyChecking=no localhost uptime
ssh -oStrictHostKeyChecking=no 0.0.0.0 uptime

echo "creating events folder"
mkdir /tmp/spark-events

echo "Create folder Namenode"
mkdir -p /tmp/hadoop-agricore/dfs/name
 
echo "Starting HDFS"
#Create file system
printf "N" | $HADOOP_HOME/bin/hdfs namenode -format
$HADOOP_HOME/bin/hdfs --daemon start namenode > ./start-namenode.log

# Now, start YARN resource manager and redirect output to the logs
echo "Starting YARN resource manager"
yarn resourcemanager > ~/resourcemanager.log 2>&1 &

echo "Starting Job History Server"
$HADOOP_HOME/bin/mapred --daemon start historyserver > ./start-historyserver.log

# Next, start Spark mastercd
echo "Starting Spark master..."
exec start-master.sh -h agricoredwh-master -p 7077 &

echo "Starting Spark history server..."
hdfs dfs -mkdir -p /shared/spark-logs
exec start-history-server.sh &

echo "Starting hive..."
hdfs dfs -mkdir -p /user/hive
$HIVE_HOME/bin/schematool -dbType postgres -ifNotExists  -initSchema
$HIVE_HOME/hcatalog/sbin/hcat_server.sh start
$SPARK_HOME/sbin/start-thriftserver.sh --master "yarn"

echo "Correcting sqoop..."
wget -P $SQOOP_HOME/lib/ https://repo1.maven.org/maven2/commons-lang/commons-lang/2.6/commons-lang-2.6.jar
wget -P $SQOOP_HOME/ https://jdbc.postgresql.org/download/postgresql-42.3.5.jar
export HADOOP_CLASSPATH=$SQOOP_HOME/postgresql-42.3.5.jar

echo "Starting zeppelin...."
${ZEPPELIN_HOME}/bin/zeppelin-daemon.sh start

echo "Examples..."
hdfs dfs -mkdir -p /examples/files
hdfs dfs -put /home/agricore/examples/files/quijote.txt /examples/files
hadoop fs -chmod -R 777 /examples

# Start bash to prevent Docker image from exit
while true; do sleep 1000; done
