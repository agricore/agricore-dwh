#!/bin/bash

echo "Initializing SSH"
sudo service ssh start

echo "Create folder Datanode"
mkdir -p /tmp/hadoop-agricore/dfs/data
chown -R agricore /tmp/hadoop-agricore

echo "Starting HDFS"
$HADOOP_HOME/bin/hdfs --daemon start datanode > ./start-datanode.log

# Start Spark master
echo "Starting Spark worker..."
exec start-worker.sh -p 7177 -c 1 -m 1G spark://agricoredwh-master:7077 &

# agricore@spark-worker:~/hadoop-3.2.1/sbin$ ./yarn-daemon.sh start nodemanager
exec $HADOOP_HOME/sbin/yarn-daemon.sh start nodemanager &

# exec bash

while true; do sleep 1000; done