# Agricore-DWH

**Agricore-DWH** service purpose is the data storage system for all the information compiled from the **Agricore-Indexers**, as well as the data source processing by ETL operations.

v0.3

# Service execution

The service could be executed by docker. 

1.- To facilitate queries/testing of the different websites, it has been included in the host file (Windows in this case)
    
    127.0.0.1            agricoredwh

This step is not necessary and would work perfectly with localhost.

2.- As an initial step you have to build the Docker images: Base, Master and Worker. These can be done with the help of "make" if running on Linux:

```console
make build_base_image
make build_master_image
make build_worker_image
make build_jupyter_image
```
or by doing the build directly if running from Windows:

```console
docker build -f ./Dockerfile . -t spark_base_image
docker build -f ./master/Dockerfile . -t spark_compose_master
docker build -f ./worker/Dockerfile . -t spark_compose_worker
docker build -f ./jupyter/Dockerfile . -t spark_compose_jupyter
```

3.- The cluster will be built with the following command:

```console
docker compose up
```

4.- Several containers will be created and running, with different functionalities

- agricoredwh-master-1: will act as the main node, we should only have one in the whole cluster.
- agricoredwh-worker1-1: will act as a secondary node, it can be replicated as many times as we need nodes in the cluster.
- agricoredwh-db-1: makes available an instance of the Postgres database. 
- agricoredwh-jupyter-1: Jupyter Lab application.

Bash containers

```console
docker exec -it agricoredwh-master-1 /bin/bash
docker exec -it agricoredwh-worker1-1 /bin/bash
docker exec -it agricoredwh-db-1 /bin/bash
docker exec -it agricoredwh-jupyter-1 /bin/bash
```

# TOOLKIT

The service launchs several containers for different purposes.

##UBUNTU (v20.04):


##HDFS (v3.2.1):

  - agricoredwh-master-1

    - *HDFS Administrator Web UI* (http://agricoredwh:9870/)
    - *Namenode* (hdfs://agricoredwh:9000/): Is the master or main node of the system. It is not responsible for storing the data itself, but for managing its access and storing its metadata.


  - agricore-worker1-1
    - *Datanode* (hdfs://agricoredwh:9864/): Is responsible for writing and reading data, executing commands from the NameNode to create, delete and replicate blocks.

  
##YARN (v3.2.1):

  - agricoredwh-master-1

    - *YARN Administrator Web UI- Resource Manager* (http://agricoredwh:8088/): Manages the distribution of cluster resources.
    - *MapReduce JobHistory Server Web UI* (http://agricoredwh:19888/jobhistory): 

  - agricore-worker1-1

    - *YARN Node Manager* (http://agricoredwh:8042): Provides the necessary computational resources for applications in the form of containers and monitors the allocation of resources (cpu, memory, disk and network).


##SPARK (v3.2.2):

  - agricoredwh-master-1

    - *Spark Master Web UI*: http://agricoredwh:8181/
    - *Spark History Web UI*: http://agricoredwh:18080/
    - *Spark Master*: http://agricoredwh:7077/

  - agricore-worker1-1
    - *Spark Worker* (http://agricoredwh:7177/): are responsible for executing spark application processes.

OPEN SPARK SHELL

To launch a spark shell to operate with the spark cluster, execute the following command
``` console
docker exec -ti agricoredwh-master-1 /home/agricore/spark/bin/spark-shell scala
 ```
Or a pyspark shell
``` console
docker exec -ti agricoredwh-master-1 /home/agricore/spark/bin/pyspark
 ```

##HIVE (v3.1.2):

  - agricoredwh-master-1

    - *HIVE Server*: jdbc:hive2://localhost:10000
    - *HIVE Metastore URI*: thrift://localhost:9083
  
METASTORE

The metastore will be created in Postgres at the first start of the cluster. Accessing beeline will be done with:

```console
beeline -n agricore --showHeader=true -u jdbc:hive2://localhost:10000
```

OPEN HIVE SHELL

To launch a hive shell to operate with the hive HDFS data, execute the following command

``` console
docker exec -ti agricoredwh-master-1 hive
 ```

##SQOOP (v1.4.7):

  Sqoop has a number of command line functionality available. It will facilitate the transfer of data between HIVE or HDFS and relational databases.

It correctly performs the connection with the DB. Here are some examples of how it works.

```console
sqoop list-tables --connect jdbc:postgresql://172.18.0.1:5432/postgres --username postgres --password postgres
sqoop-import --connect jdbc:postgresql://172.18.0.1:5432/postgres --username postgres --password postgres --table test_table --split-by name --delete-target-dir --target-dir /test_sqoop --bindir ~/sqoop-1.4.7.bin__hadoop-2.6.0/lib/ 
```

##ZEPPELIN (v0.10.1):

  - agricoredwh-master-1
    - *Zeppelin Web UI* (http://agricoredwh:8090/)

For the execution of processes taking YARN as Master it is necessary to change in the Spark interpreter properties the properties 'spark.master' to 'yarn' and spark.submit.depliyMode to 'client'. Without these changes, the processes would run in "local" mode.



##POSTGRESQL (vEdge):

  - The Postges DB will be exposed at 172.18.0.21:5432. Additionally indicate that it will contain the Hive metastore
    * POSTGRES_PASSWORD=postgres
    * POSTGRES_USER=postgres
    * POSTGRES_DB=postgres

  - agricoredwh-db-1 container

##JUPYTER LAB (v3.4.3):

  - *Jupyter Lab Web UI*: http://agricoredwh:8888/lab?token=hi
  - agricoredwh-jupyter-1 container
  

# Estructura del repositorio

* conf-master: stores the master configuration files.
* conf-worker: stores the worker configuration files.
* conf-zeppelin: stores the zeppelin configuration files.
* docs: project documentation 
* jupyter: contains the jupyter Dockerfile and files config
* master: contains the master Dockerfile
* worker: contains the worker Dockerfile
* scripts: configuration scripts to run servers.
* shared-jupyter: this repository is shared between the jupyter Docker container (/home/jovyan/work) and the host.
* shared-master: this repository is shared between the master Docker container (/home/agricore/shared) and the host.
* shared-worker: this repository is shared between worker Docker containers (/home/agricore/shared) and host.

# NETWORKS

- Master: 172.18.0.20
- Worker: 172.18.0.2
- DB: 172.18.0.21

# TESTS AND EXAMPLES

##SparkPi(scala) or piPy(python)
To verify that the cluster was installed correctly, the following processes can be run in master node: SparkPi(scala) or piPy(python). As follows:

```console
spark-submit --class org.apache.spark.examples.SparkPi --master yarn --deploy-mode cluster --driver-memory 1g --executor-memory 1g --executor-cores 1 $SPARK_HOME/examples/jars/spark-examples*.jar 1000
spark-submit --master yarn $SPARK_HOME/examples/src/main/python/pi.py 1000
```

Process file content 'quijote.txt' through the Spark cluster and save results in remote HDFS

```console
spark-submit  --master yarn --deploy-mode cluster --name "WordCount" /home/agricore/examples/spark_word_count/WordCount.py /quijote.txt /output_spark_wordcount
```
View output in HDFS/output_spark_wordcount

## JUPYTER

In the examples folder of Jupyter there are several preloaded examples

###WordCount

Process file content 'quijote.txt' through the Spark cluster and save in remote HDFS

###SaveCsvHdfs

Save csv local file 'phone-number-pricing.csv' in Remote HDFS

## Download a database into the DWH using sqoop

To download a database into the DWH, the sqoop utility must be used. In the current DWH version, a sample mysql database is deployed for testing purposes. 

For export a table from a database and storing it into the DWH, execute the following commands:

``` console
agricore@agricore-dwh:~$ docker exec -ti hiveserver bash
docker exec -ti agricoredwh-master-1 hive
root@XXX: sqoop import --connect jdbc:mysql://mysql:3306/employees --username root --password college --table employees --target-dir /sqoop/hive/employees --hive-import
``` 

More available commands for sqoop could be used for more specific tasks such as a full database download operation. https://sqoop.apache.org/docs/1.4.7/SqoopUserGuide.html

Now, the imported data is available in the DWH. Go to _Hadoop HDFS_ (http://localhost:9870) and navigate to /user/hive/warehouse/employees.

The data could be queried by the hive shell.
``` console
agricore@agricore-dwh:~$ docker exec -ti hiveserver bash
root@XXX: hive
hive> select * from default.employees;
``` 

In order to import a full database, it is recommended to create a hive database first where the SQL database is going to be exported.

``` console
agricore@agricore-dwh:~$ docker exec -ti hiveserver hive -e "CREATE DATABASE IF NOT EXISTS example_mysql_employees;"
agricore@agricore-dwh:~$ docker exec -ti hiveserver bash
root@XXX: sqoop import-all-tables --connect jdbc:mysql://mysql:3306/employees --username root --password college --hive-database example_mysql_employees --hive-import -m 1
```
