# Get base image
FROM spark_base_image:latest

# set the IP address Spark binds to on this node
RUN SPARK_LOCAL_IP="$(hostname --ip-address)"

#Copy examples
COPY ./examples /home/agricore/examples

# Copy start script 
COPY ./scripts/run_master.sh /home/agricore/run.sh
USER root 
RUN chmod +x /home/agricore/run.sh 

RUN echo "agricore ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

USER agricore

# Create SSH key for passwordless access
RUN ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa
RUN cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
RUN chmod 0600 ~/.ssh/authorized_keys

#COPY FILES CONFIGURATIONS

COPY ./conf-master/workers $HADOOP_CONF_DIR
COPY ./conf-master/core-site.xml $HADOOP_CONF_DIR
COPY ./conf-master/yarn-site.xml $HADOOP_CONF_DIR
COPY ./conf-master/spark-defaults.conf ./spark/conf
COPY ./conf-master/hive-site.xml ${HIVE_CONF_DIR}

########################################################
##HIVE
########################################################
RUN mkdir -p "${HIVE_HOME}/hcatalog/var/log" 
RUN mkdir -p "${HIVE_HOME}/var/log" 
RUN mkdir -p "${HIVE_CONF_DIR}" 
RUN chmod 777 "${HIVE_HOME}/hcatalog/var/log" 
RUN chmod 777 "${HIVE_HOME}/var/log"
RUN sudo mkdir -p /data/hive/
RUN sudo chmod -R 777 /data
RUN rm ~/apache-hive-3.1.2-bin/lib/guava-19.0.jar
RUN cp ~/hadoop-3.2.1/share/hadoop/hdfs/lib/guava-27.0-jre.jar ~/apache-hive-3.1.2-bin/lib/

#CREATE SYSTEM FILE
RUN $HADOOP_HOME/bin/hdfs namenode -format


########################################################
##ZEPPELIN
########################################################
RUN wget https://dlcdn.apache.org/zeppelin/zeppelin-${ZEPPELIN_VERSION}/zeppelin-${ZEPPELIN_VERSION}-bin-all.tgz -q -O ~/zeppelin-${ZEPPELIN_VERSION}-bin.tgz
RUN tar -xvzf ~/zeppelin-${ZEPPELIN_VERSION}-bin.tgz

ADD ./conf-zeppelin/zeppelin-env.sh ${ZEPPELIN_HOME}/conf/

# ssh
EXPOSE 22
# YARN ports
EXPOSE 8030
EXPOSE 8031
EXPOSE 8032
EXPOSE 8033
# Interfaz YARN
EXPOSE 8088
EXPOSE 10020
EXPOSE 19888
EXPOSE 50070
# zeppelin
EXPOSE 8090
# Spark
EXPOSE 7077